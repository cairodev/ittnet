<?php
$stmt = $DB_con->prepare("SELECT * FROM heads");
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	extract($row);
}
?>
<title>Dashboard ITTNET</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="<?php echo $keywords; ?>" name="keywords">
<meta content="<?php echo $description; ?>" name="description">
<meta name="author" content="Cairo Felipe Developer">

<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:url" content="https://www.ittnet.net.br/" />
<meta property="og:image" content="https://www.ittnet.net.br/assets/img/imgog.jpg" />
<meta property="og:description" content="<?php echo $description; ?>" />

<link rel="stylesheet" href="./assets/css/style.css">
<link href="../assets/img/favicon.png" rel="icon">
<link href="../assets/img/favicon.png" rel="apple-touch-icon">
<script src="https://cdn.tailwindcss.com"></script>
