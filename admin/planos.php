<?php
session_start();
if (isset($_SESSION['logado'])) :
else :
  header("Location:login.php");
endif;
require "../db_config.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<?php include "components/heads.php" ?>
	<script src="https://cdn.tailwindcss.com"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" href="./assets/css/swiper.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
	<script>
		tailwind.config = {
			theme: {
				extend: {
					colors: {
						color1: 'rgba(188, 0, 202, 1)',
						color2: 'rgba(40, 14, 124, 1)',
						color3: 'rgb(62, 206, 221)',
					}
				}
			}
		}
	</script>
</head>

<body>
  <?php include "components/sidebar.php" ?>
  <div class="ml-auto mb-6 lg:w-[75%] xl:w-[80%] 2xl:w-[85%]">
    <?php include "components/header.php" ?>
    <div class="px-6 pt-6 2xl:container">
      <button class="rigth bg-green-600 text-white px-3 py-2 rounded-md my-2">
        Adicionar Plano
      </button>
      <div class="pt-4 grid gap-6 md:grid-cols-2 lg:grid-cols-3">
        <?php
        $stmt = $DB_con->prepare("SELECT * FROM products");
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          extract($row);
        ?>
          <div class="md:col-span-2 lg:col-span-1">
            <div class="<?php echo $bg; ?> max-w-lg p-6 mx-auto text-center text-gray-900 rounded-3xl shadow">
              <div class="bg-white rounded-2xl">
                <h3 class="mb-4 text-3xl font-black py-2 text-transparent bg-clip-text <?php echo $bg; ?>"><?php echo $name; ?></h3>
              </div>
              <div class="flex items-baseline justify-center my-8">
                <span class="mr-2 lg:text-5xl text-3xl text-white font-extrabold">R$ <?php echo $price; ?></span>
                <span class="text-white">/mês</span>
              </div>
              <!-- List -->
              <ul role="list" class="mb-8 space-y-4 text-left">
                <li class="flex items-center space-x-3">
                  <!-- Icon -->
                  <svg class="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
                  </svg>
                  <span class="text-white">Wi-fi de alta performance</span>
                </li>
                <li class="flex items-center space-x-3">
                  <!-- Icon -->
                  <svg class="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
                  </svg>
                  <span class="text-white">Otimizado p/ Streaming</span>
                </li>
                <li class="flex items-center space-x-3">
                  <!-- Icon -->
                  <svg class="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
                  </svg>
                  <span class="text-white">Perfeito p/ Games</span>
                </li>
              </ul>
              <a href="https://api.whatsapp.com/message/PFFOFLPW5U37H1" class="text-white <?php echo $bg_btn ?> focus:ring-4 font-extrabold rounded-2xl text-2xl px-10 py-2.5 text-center">assine já</a>
            </div>
            <button class="bg-color2 text-white px-3 py-2 rounded-md my-2">
              editar
            </button>
            <button class="bg-red-600 text-white px-3 py-2 rounded-md my-2">
              excluir
            </button>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</body>

</html>