<?php
session_start();
require '../db_config.php';
if (isset($_SESSION['logado'])) :
else :
	header("Location:login.php");
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php include "components/heads.php" ?>
	<script src="https://cdn.tailwindcss.com"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" href="./assets/css/swiper.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
	<script>
		tailwind.config = {
			theme: {
				extend: {
					colors: {
						color1: 'rgba(188, 0, 202, 1)',
						color2: 'rgba(40, 14, 124, 1)',
						color3: 'rgb(62, 206, 221)',
					}
				}
			}
		}
	</script>
</head>

<body>
	<?php include "components/sidebar.php" ?>
	<div class="ml-auto mb-6 lg:w-[75%] xl:w-[80%] 2xl:w-[85%]">
		<?php include "components/header.php" ?>
		<div class="px-6 pt-6 2xl:container">
			<div class="grid gap-6 md:grid-cols-2 lg:grid-cols-3 pb-6">
				<div class="md:col-span-2 lg:col-span-1">
					<a href="banners.php">
						<div class="h-full py-8 px-6 space-y-6 rounded-xl text-2xl border border-gray-200 bg-gradient-to-r from-color1 to-color2 text-white">
							<i class="bi bi-images"></i>
							BANNERS
						</div>
					</a>
				</div>
				<div class="md:col-span-2 lg:col-span-1">
					<a href="planos.php">
						<div class="h-full py-8 px-6 space-y-6 rounded-xl text-2xl border border-gray-200 bg-gradient-to-r from-color1 to-color2 text-white">
							<i class="bi bi-cart-fill"></i>
							PLANOS
						</div>
					</a>
				</div>
				<div class="md:col-span-2 lg:col-span-1">
					<a href="usuarios.php">
						<div class="h-full py-8 px-6 space-y-6 rounded-xl text-2xl border border-gray-200 bg-gradient-to-r from-color1 to-color2 text-white">
							<i class="bi bi-people-fill"></i>
							USUÁRIOS
						</div>
					</a>
				</div>
			</div>
			<?php include "components/analytics.php" ?>
		</div>
	</div>
</body>

</html>