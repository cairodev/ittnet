<?php
session_start();
if (isset($_SESSION['logado'])) :
else :
  header("Location:login.php");
endif;
require "../db_config.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php include "components/heads.php" ?>
  <script src="https://cdn.tailwindcss.com"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  <link rel="stylesheet" href="./assets/css/swiper.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
  <script>
    tailwind.config = {
      theme: {
        extend: {
          colors: {
            color1: 'rgba(188, 0, 202, 1)',
            color2: 'rgba(40, 14, 124, 1)',
            color3: 'rgb(62, 206, 221)',
          }
        }
      }
    }
  </script>
</head>

<body>
  <?php include "components/sidebar.php" ?>
  <div class="ml-auto mb-6 lg:w-[75%] xl:w-[80%] 2xl:w-[85%]">
    <?php include "components/header.php" ?>
    <div class="px-6 pt-6 2xl:container">
      <button class="rigth bg-green-600 text-white px-3 py-2 rounded-md my-2">
        Adicionar Usuário
      </button>
      <div class="pt-4 grid gap-6 md:grid-cols-2 lg:grid-cols-3">
        <?php
        $stmt = $DB_con->prepare("SELECT * FROM users");
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          extract($row);
        ?>
          <div class="md:col-span-2 lg:col-span-1">
            <h1 class="text-center text-lg font-bold text-color2"><?php echo $name; ?></h1>
            <img class="w-full rounded-md" src="./uploads/users/<?php echo $img ?>" />
            <button class="bg-color2 text-white px-3 py-2 rounded-md my-2">
              editar
            </button>
            <button class="bg-red-600 text-white px-3 py-2 rounded-md my-2">
              excluir
            </button>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</body>

</html>