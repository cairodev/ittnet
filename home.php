<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php include "components/heads.php"; ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" href="./assets/css/swiper.css">
</head>

<body>
	<?php include "components/nav.php"; ?>
	<?php include "components/hero.php"; ?>
	<?php include "components/banners.php"; ?>
	<?php include "components/plans.php"; ?>
	<?php include "components/apps.php"; ?>
	<?php include "components/location.php"; ?>
	<?php include "components/footer.php"; ?>

	
	<script src="./assets/js/script.js"></script>
	<script src="https://unpkg.com/flowbite@1.4.1/dist/flowbite.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
	<script>
		var swiper = new Swiper(".swiper_banners", {
			slidesPerView: 1.1,
			centeredSlides: true,
			spaceBetween: 30,
			loop: true,
			freeMode: true,
			pagination: {
				el: ".swiper-pagination",
				clickable: true,
			},
		});
	</script>
	<script>
		var swiper = new Swiper(".swiper_products", {
			centeredSlides: true,
			loop: true,
			freeMode: true,
			pagination: {
				el: ".swiper-pagination",
				clickable: true,
			},
			breakpoints: {
				300: {
					slidesPerView: 1.5,
					spaceBetween: 30,
				},
				640: {
					slidesPerView: 2,
					spaceBetween: 30,
				},
				768: {
					slidesPerView: 3,
					spaceBetween: 30,
				},
				1024: {
					slidesPerView: 3,
					spaceBetween: 30,
				},
			},
		});
	</script>
</body>

</html>