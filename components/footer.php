<footer class="bg-gradient-to-r from-color1 to-color2 mt-10 p-5">
  <div class="max-w-6xl p-4 py-6 mx-auto lg:py-16 md:p-8 lg:p-10">
    <div class="grid grid-cols-1 gap-8 md:grid-cols-3 lg:grid-cols-4">
      <div>
        <div class="lg:pt-0 pb-5 flex">
          <img src="assets/img/novalogo.png" width="150px">
        </div>
        <p class="text-white text-sm">Presente no mercado de telecomunicações há mais
          de 25 anos, a ITTNET é a maior provedora local de
          internet do estado do Piauí, atendendo residências,
          empresas, provedores e órgãos públicos.
        </p>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-white uppercase">
        www.ittnet.net.br</h3>
        <ul class="text-white">
          <li class="mb-4">
            <a href="#planos" class="hover:underline">Planos</a>
          </li>
          <li class="mb-4">
            <a href="#apps" class="hover:underline">Apps
          </li>
          <li class="mb-4">
            <a href="<?php echo $URI->base("admin/login.php"); ?>" class="hover:underline">Login</a>
          </li>
        </ul>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-white uppercase">Contato</h3>
        <p class="text-white">
          Avenida Miguel Rosa, 1650
          Norte - Centro
          Teresina PI, 64000-480
          Telefone: 3131-8000
          Email: vendas@ittnet.com.br
        </p>
      </div>
      <div>
        <i class="bi bi-whatsapp"></i>
        <i class="bi bi-instagram"></i>
        <i class="bi bi-facebook"></i>
      </div>
    </div>
    <div class="text-center pt-10">
      <span class="block text-sm text-center text-white">© ITTNET a sua internet
      </span>
      <span class="block text-sm text-center text-white">Web Developer - @cairofelipedev
      </span>
    </div>
  </div>
</footer>