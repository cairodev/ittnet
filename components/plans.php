<section class="swiper swiper_products products max-w-screen-xl mx-auto pt-10" id="planos">
		<div class="swiper-wrapper">
			<?php $stmt = $DB_con->prepare("SELECT * FROM products order by id desc");
			$stmt->execute();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
			?>
				<div class="swiper-slide">
					<div class="<?php echo $bg; ?> max-w-lg p-6 mx-auto text-center text-gray-900 rounded-3xl shadow">
						<div class="bg-white rounded-2xl">
							<h3 class="mb-4 text-3xl font-black py-2 text-transparent bg-clip-text <?php echo $bg; ?>"><?php echo $name; ?></h3>
						</div>
						<div class="flex items-baseline justify-center my-8">
							<span class="mr-2 lg:text-5xl text-3xl text-white font-extrabold">R$ <?php echo $price; ?></span>
							<span class="text-white">/mês</span>
						</div>
						<!-- List -->
						<ul role="list" class="mb-8 space-y-4 text-left">
							<li class="flex items-center space-x-3">
								<!-- Icon -->
								<svg class="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
								</svg>
								<span class="text-white">Wi-fi de alta performance</span>
							</li>
							<li class="flex items-center space-x-3">
								<!-- Icon -->
								<svg class="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
								</svg>
								<span class="text-white">Otimizado p/ Streaming</span>
							</li>
							<li class="flex items-center space-x-3">
								<!-- Icon -->
								<svg class="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
								</svg>
								<span class="text-white">Perfeito p/ Games</span>
							</li>
						</ul>
						<a href="https://api.whatsapp.com/message/PFFOFLPW5U37H1" class="text-white <?php echo $bg_btn ?> focus:ring-4 font-extrabold rounded-2xl text-2xl px-10 py-2.5 text-center">assine já</a>
					</div>
				</div>
			<?php
			}
			?>
		</div>
	</section>